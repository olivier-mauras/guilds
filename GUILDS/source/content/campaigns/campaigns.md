<div> <h1>Campaigns</h1> </div>

_Welcome Guildmaster. Mondus is in turmoil: Rotvar, the land of mortals, lives in chaos amidst wars and monsters of all kinds._
_At the dawn of the 6th Era a new great threat looms._  

_The Tyrant-God Tialevor, after slaying the Dragon-God Ghomir and estabilish a reign of terror in Aera, the land of the immortals, is aeger to conquer the rest of the world.  
Thanks to the Astral Portals, the Tyrant-God's troops are finally able to cross the spiritual barrier between Aera and Rotvar, the barrier created by the Dragon-God sacrifice to defend mortals from Tialevor._  

_As Guildmaster you will lead your Guild, and you will always decide for which ideals your companions will live or die!_  

_Will you fight to save the world from the looming darkness or to subject it to your will?_
_Will you raid to replenish your guild treasury and satiate your greed?_
_Will you protect the lands of your ancestors from corruption?_
_As Guildmaster you will experience fantastic adventures, and it is you and only you who decide what to fight for._  

_Well the time for talking is over._  
_Have a good voyage Guildmaster, let your adventure begin!_

Campaign Special Rules
======================

Campaigns are a sequence of thematic Quests.
You can start and play with your friend a full Campaign or play a single Quest from this Chapter, either your favorite or a random one.  

At the beginning of each Campaign some Special Rules may affect extra features or limits of all the Quest belonging to it.  

When playing a Campaign you start with a small rookie Guild which will grow and improve between a Quest and another thanks to the Quest Rewards, obtained through Victory Points (VP).

## Campaign Deck

Unless specified differently in a Campaign, each player starts the first Quest of the Campaign only with a Great Hall card.  
As explained in the Rules, it provides an additional Guildhall Card and few Model Cards.  
As descripted in the Rules, model Vanquished during a Quest will come back for the following one.

## Winning a Campaign

The winner of a Campaign is the Guild who won the last Quest.
However, winning the previous Quest and completing achievement lets you gain more Rewards, thus getting more Model Cards, more Guildhall Cards, Spells, Artifacts and so on.

Victory Points
==============

Unless specified differently, this is how to normally obtain Victory Point during any Campaign’s Quest.

* +1 VP by Vanquishing an opponent Hero (no GM), Heroic Mount or Heroic Beast.
* +2 VP by Vanquishing an opponent GM.
* +1 VP by Vanquishing all models belonging to an opponent Model Card.
* +3 VP by Vanquishing all models of an opponent Guild.
* +3 VP if none of your models are Vanquished by the end of the Quest.

```{note}
Models that are Summoned into the game board thanks to Spells or Abilities are not counted in the calculation of VP unless otherwise specified in the special rules.
```

## Upgrading your Guild

Victory Points let you buy Guildhall Cards to upgrade your Guild: you can get an additional Guildhall Card for each 3 Victory Points gained.  

Certain Quests let you gain extra Victory Points by performing certain tasks.

<div class="break-page"></div>
