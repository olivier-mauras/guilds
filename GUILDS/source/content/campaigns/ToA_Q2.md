## The Ancient Ritual

<div><h3>Campaign 1 - Quest 2</h3></div>

_After a grueling battle one of the contending Guilds has finally succeeded in getting inside the Temple of Arba._  
_While the losers retreat and call for reinforcements, a Guild wanders for hours in the ancient ruins, fighting rotten skeletons and spiders.
The treasure room and its majestic Altar, as described in the ancient text, is finally found.
After a few days, the scholars managed to decipher the mystical formula.
Once pronounced, the Crown of Arba appears, as does the rival Guild crossing the threshold of the altar room.
The battle was inevitable!_

### Quest Map

![ToA Q2 Map](../../_static/campaigns/ToA_Q2.png)

### Special Rules

* The Altar is a 8” ray circular Area and is located at the center of the Table. This area is raised 2” above the ground.
* **Deployment**: The winner of the previous quest (P1) deploys first within 20" of his side of the table. The second player (P2) must deploy models within 8"of the opposite side, at least 6" away from the Altar Area.<br/> In case of a draw in the previous Quest or if this Quest is played outside of the Campaign,the player Rolls Off.

### Event Phase

Because of the spell to free the treasure, the temple is in danger of collapsing. 
During the Event Phase, the last Player of the previous Round rolls 3D6, Events occurs following the table below:

* **Double**: All models in the Altar area perform a Dex test: models that fail the test are Stunned. Heroes and Heroic Mounts may try an Heroic Active Dodge Reaction and succeed to avoid this effect.
* **Triple**: All models in the Altar Area performa Dex test: models that fail the test are Stunned and are Vanquished. Heroes and Heroic Mounts may try an Heroic Active Dodge Reaction and succeed to avoid this effect.

### Main Objective

* The Guild who controls the Altar Area at the end of 5th Round wins.

### Quest Rewards

* **Quest Participation**: +1 Guildhall Card.
* **Quest Winner**: +1 Guildhall Card.

<div class="break-page"></div>

