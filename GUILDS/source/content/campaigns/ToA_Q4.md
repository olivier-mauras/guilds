## The Depth Dragon

<div><h3>Campaign 1 - Quest 4</h3></div>

_Arba's crown has finally shown its incredible powers.
While the two guilds continue their furious battle, the treasure has attracted nefarious and ancient evils.
Hordes of skeletons emerge from the earth, perhaps ancient protectors of the crown, and even more serious, a Delani, Dragon of the Depths, has emerged from the jaws of the earth.
The Delani awakened from your battles is now hungry!_  
_One last epic battle awaits, can you survive it?
The bards will sing the exploits of the fearless Guild who defied the arcane, found a sacred relic, defeated powerful opponents and eliminated a Delani, terror of the Depths!_

### Quest Map

![ToA Q4 Map](../../_static/campaigns/ToA_Q4.png)

### Special Rules

* The Epic Boss Delani is located at the center of the Table. If the Delani is not Vanquished by the end of the Quest, both Guilds lose the game.
* This Quest is meant to be played in 5 rounds; if all players agree, the quest can last 10 rounds.
* **Deployment**: The Two Guilds must deploy to the opposite side of Quest Map, within 10" of your side and at least 15" from your opponent's models, at least 7" away from the Epic boss.
* **Spawn Points**: 2 Spawn Points must be placed 10"away from the empty corners.

### Event Phase

During the Event Phase, the last Player of the previous Round rolls 3D6:

* **Double**: 2 Skeletons are deployed at each Spawn Point.
* **Triple**: 4 Skeletons are deployed at each Spawn Point.
* **Scale**: 6 Skeletons are deployed at each Spawn Point.

### Main Objective

* The Guild who scores more Victory Points wins the Quest.

### Extra Victory Points

* +3 VP to the player who kills the Boss.
* +1 VP for every 6 Skeleton Monsters defeated.

### Quest Rewards

* **Quest Winner**: The Guild who wins this Quest wins the Campaign!

### Quest Monsters

The following Monster Cards are used in this Quest:

<div align="center">
<table>
   <tbody>
      <tr>
         <td colspan=4 class="title"><h3>Skeleton</h3><i>Monster, Fallen One</i></td>
      </tr>
      <tr>
         <td colspan=1><b>Might</b><br/>1</td>
         <td colspan=1><b>DEX</b><br/>4</td>
         <td colspan=1><b>DEF</b><br/>3</td>
         <td colspan=1><b>WILL</b><br/>3</td>
      </tr>
      <tr>
         <td colspan=4>Rusty Sword&nbsp;&nbsp;&nbsp;&nbsp;  <b>1 ATK</b>&nbsp;&nbsp;&nbsp;&nbsp;   0"</td>
      </tr>
      <tr>
         <td colspan=4>Charge Nearest</td>
      </tr>
   </tbody>
</table>
</div>

<br/>
<div class="break-page"></div>

<div align="center">
<table>
   <tbody>
      <tr>
         <td colspan=5 class="title"><h3>Delani, the Depth Dragon</h3><i>Epic Boss, Epic Beast, Towering, Dragon</i></td>
      </tr>
      <tr>
        <td colspan=5 class="list">
          <ul class="in-card">
            <li class="in-table"><b>1/2:</b> Dragon's Jaws<br/>Charge Nearest. If the ATK roll results in a Scale, the target model is swallowed (immediately Vanquished)! Scale inflicts a Heroic Wounds to Heroic Models.</li>
            <li class="in-table"><b>3/4:</b> Tail Sweep<br/>Rolls 3 ATK AoE against all models within 4" of the Delani. Inflicts 1 Wound per Success. Only affect models in Vision. The models wounded are Stunned.</li>
            <li class="in-table"><b>5:</b> Roar of the Depths<br/>Each model within a 12" who doesn’t surpass a WILL Test is Terrified.</li>
            <li class="in-table"><b>6:</b> Earthquake<br/>Charge Nearest. Roll 3 ATK AoE on all models within 8" of the Delani. Inflicts 1 Wound per Success, wounded enemies are Stunned. Levitating models are immune to this Ability.</li>
          </ul>
        </td>
      </tr>
      <tr>
         <td colspan=1><b>HW</b><br/>8</td>
         <td colspan=1><b>Might</b><br/>7(R)</td>
         <td colspan=1><b>DEX</b><br/>5(M)</td>
         <td colspan=1><b>DEF</b><br/>5</td>
         <td colspan=1><b>WILL</b><br/>10</td>
      </tr>
      <tr>
         <td colspan=5>Claws & Fangs&nbsp;&nbsp;&nbsp;&nbsp;   <b>6 ATK</b>&nbsp;&nbsp;&nbsp;&nbsp;   0"</td>
      </tr>
      <tr>
         <td colspan=5><b>Ignited (6 HW)</b><br/>Delani Heals from 1 Heroic Wounds when it Vanquishes a model.</td>
      </tr>
   </tbody>
</table>
</div>

<br/>

<div class="break-page"></div>

