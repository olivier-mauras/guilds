## Bring Back the Treasure!

<div><h3>Campaign 1 - Quest 3</h3></div>

_At last the treasure has been found: it is the legendary Crown of Arba, a sacred relic blessed by the Goddess herself and hidden in the depths of Rotvar before she was annihilated by Tialevor.
Ancient legends say that it was able to ward off all evil.
However, it’s still not active. 
Paintings in the walls suggest that a ritual involving runes is required to activate the artifact.
After another day spent with the scholar of the Guild, a solution is found: the Crown must be purified in a sacred water pool hidden in the temple._  
_Once again, the rival Guild stands before you, who will manage to seize and bring out the treasure of the Goddess Arba._

### Quest Map

![ToA Q3 Map](../../_static/campaigns/ToA_Q3.png)

### Special Rules

* The Temple Exit is a line of 10” located on the center of a side on the Quest Map.
* **Deployment**: The winner of the previous quest (P1) deploys first within 10" on the opposite side of the Exit line. The second player (P2) must deploy models within 20"of the opposite side, at least 8" away from P1.<br/>In case of a draw in the previous Quest or if this Quest is played outside of the Campaign,the player Rolls Off.
* **Arba’s Crown**: P1 controls the Tiny Treasure Arba's Crown.  Assign it to a Infantry or Hero model of your Guild.

<div class="break-page"></div>

### Main Objective

* P1 player must manage to get Arba’s Crown out of the game board through the Exit line by the end of the Quest.
* P2 must Control _Arba's Crown_ during the End of the 5th Round.

### Quest Rewards

* **Quest Participation**: +1 Guildhall Card.
* **Quest Winner**: +1 Guildhall Card.
* **Guild who accomplish the Main Objective**: Arba’s Crown Artifact Card is bound to a Hero.

<div align="center">
<table>
   <tbody>
      <tr>
         <td colspan=4 class="title"><h3>Arba’s Crown</h3><i>Artifact Card</i></td>
      </tr>
      <tr>
         <td colspan=4 class="list">Once per Round, the Hero can Activate this Artifact for free to cancel the effect of an Ability/Attack/Spell that affects this model and/or all allies within 8".</td>
      </tr>
   </tbody>
</table>
</div>
<br/>

<div class="break-page"></div>

