Monsters
========

Many monsters haunt the lands, and all of them are enemies of all the player's Guilds on the table.  
Not only Kobolds, Skeletons and Trolls are Monsters: wandering bandits and smugglers are part of this category too.  
Monsters are included in a Quest through Special Rules or Objectives: the firsts are just wandering problems, while the seconds have to be slain to win the game!  
Monsters are activated on their own, following some basic rules called Monster Behaviors.
Monster Cards are always at the end of the Quest in which the Monsters appear.

## Spawn Points

Special Rules could include a Monster Spawn Point: these Areas of the Map generate Monster under certain circumstances, from the beginning of the Quest, or even as Quest Events during the Event Phase because of Double, Triple or Scale rolls.  
In short, each Quest describes how Spawn Points work.

<div align="center">
<table>
   <tbody>
      <tr>
         <td colspan=4 class="title"><h3>Skeleton</h3><i>Monster</i></td>
      </tr>
      <tr>
         <td colspan=1><b>Might</b><br/>1</td>
         <td colspan=1><b>DEX</b><br/>4</td>
         <td colspan=1><b>DEF</b><br/>3(S)</td>
         <td colspan=1><b>WILL</b><br/>3</td>
      </tr>
      <tr>
         <td colspan=4>Spear&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <b>1 ATK</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   0-2"</td>
      </tr>
      <tr>
         <td colspan=4>Charge Nearest</td>
      </tr>
   </tbody>
</table>
</div>
<br/>

```{hint}
Example of Monster Card, specialized in the use of Shield Rolls (S) and Indirect Engagement (I)
```

## Monster Activation

The Monster Activations happen once all Players have ended their Activation, at the end of a Round.  
Monster Activations order is based on their DEX value.
Higher DEX Monsters move first, and the others follow in descending order.
Monsters move on their own thanks to the Monster Behavior.

## Monster Behaviour

Each Monster has a simple Behavior written under its name that lets you move it in the simplest way on the table by following 6 simple rules.  
If two players can’t agree on the Monster movement or which model is the target they can Roll Off.

1. Monsters always take the shortest path to accomplish their Behavior.
2. Monsters only Attack Player models, unless stated otherwise in the Special Rules or Monster Behaviors.
3. If a specific target of a Monster can’t be reached and Attacked, the Monster Attacks the Nearest enemy unless specified otherwise.
4. If there are two or more possible targets, Monsters ALWAYS Attack the one with less Might.
5. If a Monster has its target at range, it uses all possible Actions to Attack.
6. If two Behaviors are listed with a “ ; ” in the middle, the first has priority, but if it can’t be performed the second one needs to be accomplished.

## Common Behaviors

* **Charge Nearest**: the Monster’s first priority is to Move and Engage the Nearest Model, then Attack it.
* **Shoot from Max Range**: The monster adjusts its distance to its Nearest possible Target trying to Attack from its maximum range (retreating if its too near).

## Monster Attacks

The Player who controls the target of an Attacking Monster rolls its ATK dice to determine how much Wounds it inflicts.  
Apart from this, Attacks work as always.

## Monster Abilities

Exactly like regular Models or Heroes, Monsters have Abilities.  
These special features are often Passive: their effects  are always in play.
Active Abilities uses are specified in the Behaviors.

<div class="break-page"></div>
