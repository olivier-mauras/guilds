Quests
======

Quests determine where the Guild models can be Deployed, Special Rules, Events, what’s the main Objective to achieve Victory and so on.  
A quick and simple Quest to play is Guild Clash, included at the beginning of the Campaign Chapter.
Many other Quests are available in the Campaigns Chapter.
Campaigns feature a sequence of thematic Quests relevant to the plot.  

You can start and play with your friend a full Campaign or play a single Quest from this Chapter, either your favorite or a random one.  

## Quest Descriptions

Quests Descriptions helps you set up your game and understand the special rules that take place in that specific scenario.  
Every Quest also includes some little Lore to immerse yourself in the Artisan Guild universe.
A quick and simple Quest to play is _Guild Clash_, included below.

### Quest Map

Quest Maps help you Set Up the table.  
It shows each Player Side, which indicates where models can be Deployed.  
Unless otherwise specified, they need to be placed at least within 10” of your Side border and beyond 15” from your opponent's models.  
The Quest Map also includes Special Rules, Objectives or Monster Spawn areas.  
Most Quests Maps areas correspond to 30” x 30” on your table. If your table is a bit smaller, adjust distances between Objectives accordingly.

### Special Rules

Special Rules alters the way you play a Quest.  
It includes required Terrains, Monsters or certain restrictions.

### Quest Events

During the Event Phase, the last Player of the previous Round rolls 3D6: Quest Events could occur in relation to the roll as descripted in the Quest Scroll.  
For example, in a Mine Quest a Cave Troll could break into a tunnel if in the Event Phase a player rolls a Double.  
**Double**: rolls showing 2 identical results, like 4-4, 1-1, 2-2, and so on.  
**Triple**: rolls of 3 identical results, like 3-3-3.  
**Scale**: 3 dice in sequence, such 1-2-3 or 3-4-5.  


### Game Length

Quests have limits of 5 Rounds, unless specified otherwise in the Special Rules.  
When the fifth-Round ends the Quest is over: if no Guild manages to accomplish the main Objective, the Guild who scored more Victory Points won the game.  
Quest’s main Objective could also be to score more Victory Points than your opponents.  
Quests could also end as a draw.

## Objectives

Conditions to win the Quest.  
They may require one of the Players Guild to maintain control of a key point for various Rounds, or the elimination of a particular model or Monster.  
When an Objective is accomplished by a Guild, it scores a Victory, gains the Quest Reward, and the Quest ends.  
There are 4 type of Quest Objectives:  

* Terrain Objectives are fixed on the Map, and often needs to be Controlled for some Rounds.
* Treasure Objectives can be Carried by models if Controlled.
* Monster Objectives move on their own, they often need to be Vanquished.
* Conditions Objectives are circumstances that must be achieved to win and are not visible on the Map.

Physical Objectives require a marker on the table. You can use Artisan Guild Miniatures for Monsters and 3d Printed/Handmade Terrains, but any kind of token or placeholder like bottle caps or glasses works too! Terrain and Treasures distances are always measured from the center of its marker.

### Objective Control

Objective Control, such as controlling a shrine, is a common Objective. At the end of a Round, the player who has more models within 5” of the center of a Terrain Objective is controlling it, 3” for a Treasures.  
Towering Models and Guild Masters count for 2 when calculating which Guild controls an objective.  
A model within range can Control multiple Objectives.

### Treasure Gathering
Guild Controlling a Treasure Objective can carry it.  
To do that, a model Controlling the Treasure consumes 1 Action to Gather it: the Treasure is then moved on the table together with the model (touching its base), to indicate that it’s carried by it.  

Models carrying Treasures can’t perform Actions outside of Moving or Passing the Treasure unless the Treasure has the Tag Tiny, such a book, a crown or a ring.  

1 Action can be used by the Gatherer of a Treasure to Pass the Treasure to another friendly model within 3”.  
Tiny Treasures can be Thrown to a far ally at maximum of DEX in Inches by throwing it by passing a DEX Test; if failed the Tiny Treasure falls in the middle of the range, on the ground.  

If Vanquished, a model Gathering a Treasure leaves it on the ground and it could be Gathered by another model Controlling it (see Objective Control).  

Quest Objective could require a model to Collect a 1 or more Treasures: to do that Gather it and bring it to the border of the Player Side border/s.

## Quest Winner

Victory points (VP for short) are calculated after the end of each Quest following a table described at the beginning of the Campaign Chapter.  
In addition to defeating enemies, each Quest describes how to earn more Victory Points.  
If the Main Objective of the Quest is achieved, the game is automatically won, but if no Guild manages to complete the Main Objective, VP are calculated to choose a winner.  
In some scenarios the main objective of the Quest might be to get as much VP as possible.

<div class="break-page"></div>

